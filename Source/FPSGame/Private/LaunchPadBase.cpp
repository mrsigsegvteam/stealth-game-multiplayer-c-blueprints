// Fill out your copyright notice in the Description page of Project Settings.

#include "LaunchPadBase.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/PrimitiveComponent.h"
#include "FPSCharacter.h"


// Sets default values
ALaunchPadBase::ALaunchPadBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RootComponent = MeshComp;

	OverlapComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("OverlapComp"));
	OverlapComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	OverlapComponent->SetBoxExtent(FVector(200.0f));
	OverlapComponent->SetupAttachment(RootComponent);

	ArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowComp"));
	ArrowComponent->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ALaunchPadBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ALaunchPadBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ALaunchPadBase::NotifyActorBeginOverlap(AActor * OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	FVector ForwardVector = ArrowComponent->GetForwardVector();
	ForwardVector = ForwardVector + FVector(0, 0, 1000);
	ForwardVector = ForwardVector * FVector(1000, 1000, 1);

	AFPSCharacter* MyCharacter = Cast<AFPSCharacter>(OtherActor);
	if (MyCharacter)
	{
		MyCharacter->LaunchCharacter(ForwardVector,false,false);
	} 
	else
	{
		UPrimitiveComponent* PComponent = Cast<UPrimitiveComponent>(OtherActor->GetRootComponent());
		if (PComponent)
		{
			PComponent->AddImpulse(ForwardVector, EName::NAME_None, true);
		}
	}

}